-- MySQL dump 10.13  Distrib 5.7.20, for Linux (x86_64)
--
-- Host: localhost    Database: test5
-- ------------------------------------------------------
-- Server version	5.7.20-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `championship`
--

DROP TABLE IF EXISTS `championship`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `championship` (
  `id` char(32) NOT NULL COMMENT 'идентификатор',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'дата\\время создания',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='чемпионат';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `championship`
--

LOCK TABLES `championship` WRITE;
/*!40000 ALTER TABLE `championship` DISABLE KEYS */;
INSERT INTO `championship` VALUES ('332ec6c2b13da244510fdaca69aca085','2017-10-13 06:34:39'),('525054d71ebdd9262e3b3c8137a4cc1c','2017-10-13 06:33:06'),('5c991fd1fac782b9b86bea42a2941dfe','2017-10-12 04:00:30'),('8153f43ef21eaecc3ef6c403429c9bc8','2017-10-12 03:55:05'),('86b374955e6c8e6ac7e69e3d0065886b','2017-10-12 04:11:11'),('87d703dc1e7f7d64352d89002e5c707b','2017-10-12 04:03:26'),('d8925752126759cdb469a5d86ad30c71','2017-10-12 04:09:14'),('da4d939c3ac3f045fe39fc11932f706f','2017-10-12 10:38:17');
/*!40000 ALTER TABLE `championship` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `competition`
--

DROP TABLE IF EXISTS `competition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `competition` (
  `id` bigint(22) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` char(32) DEFAULT NULL COMMENT 'идентификатор родительской группы',
  `championship_id` char(32) NOT NULL COMMENT 'идентификатор сессии',
  `group_id` char(32) NOT NULL COMMENT 'идентификатор группы',
  `teamA_id` tinyint(3) unsigned NOT NULL COMMENT 'индетификатор первой комманды',
  `teamB_id` tinyint(3) unsigned NOT NULL COMMENT 'индетификатор второй комманды',
  `teamA_goals` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'голы команды "A" в розыгрыше',
  `teamB_goals` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'голы команды "B" в розыгрыше',
  `teamA_goals_total` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'всего голов команды "A"',
  `teamB_goals_total` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'всего голов команды "B"',
  `teamA_scores` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'очки команды "A"',
  `teamB_scores` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'очки команды "B"',
  `played` datetime DEFAULT NULL COMMENT 'дата\\время игры',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'дата\\время создания',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_idx` (`group_id`,`teamA_id`,`teamB_id`) USING BTREE,
  KEY `idx_team_id` (`teamA_id`,`teamB_id`) USING BTREE,
  KEY `parent_id` (`parent_id`) USING BTREE,
  KEY `fk_competition_teamB_id_team_idx` (`teamB_id`),
  KEY `fk_competition_championship_id_championship` (`championship_id`),
  CONSTRAINT `fk_competition_championship_id_championship` FOREIGN KEY (`championship_id`) REFERENCES `championship` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_competition_teamA_id_team` FOREIGN KEY (`teamA_id`) REFERENCES `team` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_competition_teamB_id_team` FOREIGN KEY (`teamB_id`) REFERENCES `team` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1859 DEFAULT CHARSET=utf8mb4 COMMENT='соревнование';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `competition`
--

LOCK TABLES `competition` WRITE;
/*!40000 ALTER TABLE `competition` DISABLE KEYS */;
INSERT INTO `competition` VALUES (1701,NULL,'8153f43ef21eaecc3ef6c403429c9bc8','1a54ce173cf24dd9f9e88fdeab338b18',2,16,0,0,0,0,0,0,NULL,'2017-10-12 03:55:06'),(1702,NULL,'8153f43ef21eaecc3ef6c403429c9bc8','1a54ce173cf24dd9f9e88fdeab338b18',27,31,0,0,0,0,0,0,NULL,'2017-10-12 03:55:06'),(1703,NULL,'8153f43ef21eaecc3ef6c403429c9bc8','79642452113e58042b549a5adf17ab0f',17,9,0,0,0,0,0,0,NULL,'2017-10-12 03:55:06'),(1704,NULL,'8153f43ef21eaecc3ef6c403429c9bc8','79642452113e58042b549a5adf17ab0f',8,29,0,0,0,0,0,0,NULL,'2017-10-12 03:55:06'),(1705,NULL,'8153f43ef21eaecc3ef6c403429c9bc8','6bf5ce3d57bd50c35e9aa9301c6db3b3',28,15,0,0,0,0,0,0,NULL,'2017-10-12 03:55:06'),(1706,NULL,'8153f43ef21eaecc3ef6c403429c9bc8','6bf5ce3d57bd50c35e9aa9301c6db3b3',18,12,0,0,0,0,0,0,NULL,'2017-10-12 03:55:06'),(1707,NULL,'8153f43ef21eaecc3ef6c403429c9bc8','90bc8b65e8fd473f3f44c4ef6e48bb63',23,30,0,0,0,0,0,0,NULL,'2017-10-12 03:55:06'),(1708,NULL,'8153f43ef21eaecc3ef6c403429c9bc8','90bc8b65e8fd473f3f44c4ef6e48bb63',21,1,0,0,0,0,0,0,NULL,'2017-10-12 03:55:06'),(1709,NULL,'8153f43ef21eaecc3ef6c403429c9bc8','7d5b3067cb9d617e00c7a8a7bfef4d00',4,14,0,0,0,0,0,0,NULL,'2017-10-12 03:55:06'),(1710,NULL,'8153f43ef21eaecc3ef6c403429c9bc8','7d5b3067cb9d617e00c7a8a7bfef4d00',26,10,0,0,0,0,0,0,NULL,'2017-10-12 03:55:06'),(1711,NULL,'8153f43ef21eaecc3ef6c403429c9bc8','853fd4837c29cd8470a7ec653bf823fb',20,13,0,0,0,0,0,0,NULL,'2017-10-12 03:55:06'),(1712,NULL,'8153f43ef21eaecc3ef6c403429c9bc8','853fd4837c29cd8470a7ec653bf823fb',24,5,0,0,0,0,0,0,NULL,'2017-10-12 03:55:06'),(1713,NULL,'8153f43ef21eaecc3ef6c403429c9bc8','d019df8e3a36a5de350ba2af4c5bb336',7,25,0,0,0,0,0,0,NULL,'2017-10-12 03:55:06'),(1714,NULL,'8153f43ef21eaecc3ef6c403429c9bc8','d019df8e3a36a5de350ba2af4c5bb336',6,11,0,0,0,0,0,0,NULL,'2017-10-12 03:55:06'),(1715,NULL,'8153f43ef21eaecc3ef6c403429c9bc8','d6f29c69b7051405cd0081552e429f11',22,32,0,0,0,0,0,0,NULL,'2017-10-12 03:55:06'),(1716,NULL,'8153f43ef21eaecc3ef6c403429c9bc8','d6f29c69b7051405cd0081552e429f11',19,3,0,0,0,0,0,0,NULL,'2017-10-12 03:55:06'),(1717,NULL,'5c991fd1fac782b9b86bea42a2941dfe','26e12f9103fc2fea2892bd2401b42597',3,23,0,0,0,0,0,0,NULL,'2017-10-12 04:00:30'),(1718,NULL,'5c991fd1fac782b9b86bea42a2941dfe','26e12f9103fc2fea2892bd2401b42597',4,19,0,0,0,0,0,0,NULL,'2017-10-12 04:00:30'),(1719,NULL,'5c991fd1fac782b9b86bea42a2941dfe','8d76fb8dcd3b80d449b15114f74674ce',30,2,0,0,0,0,0,0,NULL,'2017-10-12 04:00:30'),(1720,NULL,'5c991fd1fac782b9b86bea42a2941dfe','8d76fb8dcd3b80d449b15114f74674ce',22,32,0,0,0,0,0,0,NULL,'2017-10-12 04:00:30'),(1721,NULL,'5c991fd1fac782b9b86bea42a2941dfe','fde9f22a7fcac5a7c665c839eb5d38ae',7,28,0,0,0,0,0,0,NULL,'2017-10-12 04:00:31'),(1722,NULL,'5c991fd1fac782b9b86bea42a2941dfe','fde9f22a7fcac5a7c665c839eb5d38ae',26,1,0,0,0,0,0,0,NULL,'2017-10-12 04:00:31'),(1723,NULL,'5c991fd1fac782b9b86bea42a2941dfe','aa079ab0c8347b1e27b9ab8a8b23eb0f',8,29,0,0,0,0,0,0,NULL,'2017-10-12 04:00:31'),(1724,NULL,'5c991fd1fac782b9b86bea42a2941dfe','aa079ab0c8347b1e27b9ab8a8b23eb0f',12,27,0,0,0,0,0,0,NULL,'2017-10-12 04:00:31'),(1725,NULL,'5c991fd1fac782b9b86bea42a2941dfe','a6964c704d36e7a04e79f8a527c11703',21,16,0,0,0,0,0,0,NULL,'2017-10-12 04:00:31'),(1726,NULL,'5c991fd1fac782b9b86bea42a2941dfe','a6964c704d36e7a04e79f8a527c11703',9,10,0,0,0,0,0,0,NULL,'2017-10-12 04:00:31'),(1727,NULL,'5c991fd1fac782b9b86bea42a2941dfe','16fb64fa6a9297e02cad67eec4500aa4',24,13,0,0,0,0,0,0,NULL,'2017-10-12 04:00:31'),(1728,NULL,'5c991fd1fac782b9b86bea42a2941dfe','16fb64fa6a9297e02cad67eec4500aa4',18,6,0,0,0,0,0,0,NULL,'2017-10-12 04:00:31'),(1729,NULL,'5c991fd1fac782b9b86bea42a2941dfe','e0085788d1579c71bed19dc9b8b9e249',11,15,0,0,0,0,0,0,NULL,'2017-10-12 04:00:31'),(1730,NULL,'5c991fd1fac782b9b86bea42a2941dfe','e0085788d1579c71bed19dc9b8b9e249',25,31,0,0,0,0,0,0,NULL,'2017-10-12 04:00:31'),(1731,NULL,'5c991fd1fac782b9b86bea42a2941dfe','b4a8c6122448075db2f9f40f5ab77759',20,17,0,0,0,0,0,0,NULL,'2017-10-12 04:00:31'),(1732,NULL,'5c991fd1fac782b9b86bea42a2941dfe','b4a8c6122448075db2f9f40f5ab77759',5,14,0,0,0,0,0,0,NULL,'2017-10-12 04:00:31'),(1733,NULL,'87d703dc1e7f7d64352d89002e5c707b','7648a89632f9c64968c67afba94aec6f',8,29,0,0,0,0,0,0,NULL,'2017-10-12 04:03:26'),(1734,NULL,'87d703dc1e7f7d64352d89002e5c707b','7648a89632f9c64968c67afba94aec6f',27,28,0,0,0,0,0,0,NULL,'2017-10-12 04:03:26'),(1735,NULL,'87d703dc1e7f7d64352d89002e5c707b','aed7aef8bd65364947cf5d6be0afc5d3',20,2,0,0,0,0,0,0,NULL,'2017-10-12 04:03:26'),(1736,NULL,'87d703dc1e7f7d64352d89002e5c707b','aed7aef8bd65364947cf5d6be0afc5d3',9,1,0,0,0,0,0,0,NULL,'2017-10-12 04:03:26'),(1737,NULL,'87d703dc1e7f7d64352d89002e5c707b','550d65ca196bdf6b7c773045cf004ab3',23,32,0,0,0,0,0,0,NULL,'2017-10-12 04:03:26'),(1738,NULL,'87d703dc1e7f7d64352d89002e5c707b','550d65ca196bdf6b7c773045cf004ab3',15,18,0,0,0,0,0,0,NULL,'2017-10-12 04:03:26'),(1739,NULL,'87d703dc1e7f7d64352d89002e5c707b','451e39d93690df871297a108cb2269d3',3,7,0,0,0,0,0,0,NULL,'2017-10-12 04:03:26'),(1740,NULL,'87d703dc1e7f7d64352d89002e5c707b','451e39d93690df871297a108cb2269d3',31,11,0,0,0,0,0,0,NULL,'2017-10-12 04:03:26'),(1741,NULL,'87d703dc1e7f7d64352d89002e5c707b','9095c8a3ce2aa42d6774683a4cc65a24',22,12,0,0,0,0,0,0,NULL,'2017-10-12 04:03:26'),(1742,NULL,'87d703dc1e7f7d64352d89002e5c707b','9095c8a3ce2aa42d6774683a4cc65a24',30,6,0,0,0,0,0,0,NULL,'2017-10-12 04:03:26'),(1743,NULL,'87d703dc1e7f7d64352d89002e5c707b','f3e462014859d565a8b6bdea7fb47d6a',14,26,0,0,0,0,0,0,NULL,'2017-10-12 04:03:26'),(1744,NULL,'87d703dc1e7f7d64352d89002e5c707b','f3e462014859d565a8b6bdea7fb47d6a',10,25,0,0,0,0,0,0,NULL,'2017-10-12 04:03:26'),(1745,NULL,'87d703dc1e7f7d64352d89002e5c707b','f82c6108773b2cfbbff8e1a1e5d64ba8',4,16,0,0,0,0,0,0,NULL,'2017-10-12 04:03:26'),(1746,NULL,'87d703dc1e7f7d64352d89002e5c707b','f82c6108773b2cfbbff8e1a1e5d64ba8',17,24,0,0,0,0,0,0,NULL,'2017-10-12 04:03:26'),(1747,NULL,'87d703dc1e7f7d64352d89002e5c707b','f17699ea94450c5f6b1a8173dfdc91cb',5,19,0,0,0,0,0,0,NULL,'2017-10-12 04:03:26'),(1748,NULL,'87d703dc1e7f7d64352d89002e5c707b','f17699ea94450c5f6b1a8173dfdc91cb',21,13,0,0,0,0,0,0,NULL,'2017-10-12 04:03:26'),(1749,NULL,'d8925752126759cdb469a5d86ad30c71','f78313c074d7374d7cee25d6ee34c631',6,31,14,3,0,0,0,3,'2017-10-12 07:09:23','2017-10-12 04:09:23'),(1750,NULL,'d8925752126759cdb469a5d86ad30c71','f78313c074d7374d7cee25d6ee34c631',15,18,2,9,0,0,0,3,'2017-10-12 07:09:23','2017-10-12 04:09:23'),(1751,NULL,'d8925752126759cdb469a5d86ad30c71','088d5b677b0088043f94018543ad4bcb',32,24,5,9,0,0,0,3,'2017-10-12 07:09:23','2017-10-12 04:09:23'),(1752,NULL,'d8925752126759cdb469a5d86ad30c71','088d5b677b0088043f94018543ad4bcb',25,13,2,12,0,0,3,0,'2017-10-12 07:09:23','2017-10-12 04:09:23'),(1753,NULL,'d8925752126759cdb469a5d86ad30c71','9a0ee1fe8969d662409f094021d36a14',23,1,2,6,0,0,0,3,'2017-10-12 07:09:23','2017-10-12 04:09:23'),(1754,NULL,'d8925752126759cdb469a5d86ad30c71','9a0ee1fe8969d662409f094021d36a14',10,3,0,6,0,0,0,3,'2017-10-12 07:09:23','2017-10-12 04:09:23'),(1755,NULL,'d8925752126759cdb469a5d86ad30c71','e89ba26b87b898b7845aeccd7efbdc84',28,17,8,11,0,0,3,0,'2017-10-12 07:09:23','2017-10-12 04:09:23'),(1756,NULL,'d8925752126759cdb469a5d86ad30c71','e89ba26b87b898b7845aeccd7efbdc84',29,22,2,7,0,0,0,3,'2017-10-12 07:09:23','2017-10-12 04:09:23'),(1757,NULL,'d8925752126759cdb469a5d86ad30c71','c47841f73cf195a47d4e698de6efbeef',5,7,3,6,0,0,0,3,'2017-10-12 07:09:23','2017-10-12 04:09:23'),(1758,NULL,'d8925752126759cdb469a5d86ad30c71','c47841f73cf195a47d4e698de6efbeef',8,20,11,1,0,0,3,0,'2017-10-12 07:09:23','2017-10-12 04:09:23'),(1759,NULL,'d8925752126759cdb469a5d86ad30c71','96c82e47bf444a97875f9dcfdc5833ff',4,26,4,6,0,0,0,3,'2017-10-12 07:09:23','2017-10-12 04:09:23'),(1760,NULL,'d8925752126759cdb469a5d86ad30c71','96c82e47bf444a97875f9dcfdc5833ff',27,16,3,5,0,0,0,3,'2017-10-12 07:09:23','2017-10-12 04:09:23'),(1761,NULL,'d8925752126759cdb469a5d86ad30c71','7aa2543b5bd57afc6bae9c92d04d03d3',21,30,5,5,0,0,1,1,'2017-10-12 07:09:23','2017-10-12 04:09:23'),(1762,NULL,'d8925752126759cdb469a5d86ad30c71','7aa2543b5bd57afc6bae9c92d04d03d3',14,11,6,11,0,0,3,0,'2017-10-12 07:09:23','2017-10-12 04:09:23'),(1763,NULL,'d8925752126759cdb469a5d86ad30c71','6be4e1168eceafe5ba61cf2d26313d70',2,9,10,8,0,0,0,3,'2017-10-12 07:09:23','2017-10-12 04:09:23'),(1764,NULL,'d8925752126759cdb469a5d86ad30c71','6be4e1168eceafe5ba61cf2d26313d70',12,19,2,8,0,0,0,3,'2017-10-12 07:09:23','2017-10-12 04:09:23'),(1765,'f78313c074d7374d7cee25d6ee34c631','d8925752126759cdb469a5d86ad30c71','0a001783d270bd26af325f340c676737',31,18,0,5,0,0,3,6,'2017-10-12 07:09:23','2017-10-12 04:09:23'),(1766,'088d5b677b0088043f94018543ad4bcb','d8925752126759cdb469a5d86ad30c71','0a001783d270bd26af325f340c676737',25,24,6,5,0,0,6,3,'2017-10-12 07:09:23','2017-10-12 04:09:23'),(1767,'9a0ee1fe8969d662409f094021d36a14','d8925752126759cdb469a5d86ad30c71','0a001783d270bd26af325f340c676737',3,1,4,3,0,0,6,3,'2017-10-12 07:09:23','2017-10-12 04:09:23'),(1768,'e89ba26b87b898b7845aeccd7efbdc84','d8925752126759cdb469a5d86ad30c71','0a001783d270bd26af325f340c676737',28,22,9,4,0,0,6,3,'2017-10-12 07:09:23','2017-10-12 04:09:23'),(1769,'c47841f73cf195a47d4e698de6efbeef','d8925752126759cdb469a5d86ad30c71','0a001783d270bd26af325f340c676737',7,8,7,2,0,0,6,3,'2017-10-12 07:09:23','2017-10-12 04:09:23'),(1770,'96c82e47bf444a97875f9dcfdc5833ff','d8925752126759cdb469a5d86ad30c71','0a001783d270bd26af325f340c676737',26,16,7,7,0,0,4,4,'2017-10-12 07:09:23','2017-10-12 04:09:23'),(1771,'7aa2543b5bd57afc6bae9c92d04d03d3','d8925752126759cdb469a5d86ad30c71','0a001783d270bd26af325f340c676737',14,21,10,10,0,0,4,2,'2017-10-12 07:09:23','2017-10-12 04:09:23'),(1772,'6be4e1168eceafe5ba61cf2d26313d70','d8925752126759cdb469a5d86ad30c71','0a001783d270bd26af325f340c676737',9,19,2,2,0,0,4,4,'2017-10-12 07:09:24','2017-10-12 04:09:24'),(1773,NULL,'86b374955e6c8e6ac7e69e3d0065886b','f19fc37ab609453d950f3cee0f9a3144',1,7,8,6,0,0,3,0,'2017-10-12 07:11:15','2017-10-12 04:11:15'),(1774,NULL,'86b374955e6c8e6ac7e69e3d0065886b','f19fc37ab609453d950f3cee0f9a3144',24,9,3,3,0,0,1,1,'2017-10-12 07:11:15','2017-10-12 04:11:15'),(1775,NULL,'86b374955e6c8e6ac7e69e3d0065886b','2f154d7d0a4770c863a560b0539ea8aa',18,6,2,3,0,0,0,3,'2017-10-12 07:11:15','2017-10-12 04:11:15'),(1776,NULL,'86b374955e6c8e6ac7e69e3d0065886b','2f154d7d0a4770c863a560b0539ea8aa',12,25,7,1,0,0,3,0,'2017-10-12 07:11:15','2017-10-12 04:11:15'),(1777,NULL,'86b374955e6c8e6ac7e69e3d0065886b','cec07d2697e94e6857f18c54db0c724e',17,14,0,8,0,0,0,3,'2017-10-12 07:11:15','2017-10-12 04:11:15'),(1778,NULL,'86b374955e6c8e6ac7e69e3d0065886b','cec07d2697e94e6857f18c54db0c724e',27,30,13,1,0,0,3,0,'2017-10-12 07:11:15','2017-10-12 04:11:15'),(1779,NULL,'86b374955e6c8e6ac7e69e3d0065886b','062b587509d6b9088a2b33b52a997467',8,11,3,8,0,0,0,3,'2017-10-12 07:11:15','2017-10-12 04:11:15'),(1780,NULL,'86b374955e6c8e6ac7e69e3d0065886b','062b587509d6b9088a2b33b52a997467',10,2,3,11,0,0,3,0,'2017-10-12 07:11:16','2017-10-12 04:11:16'),(1781,NULL,'86b374955e6c8e6ac7e69e3d0065886b','b3efda64c23e921249db8d57a2f43db2',29,4,5,7,0,0,0,3,'2017-10-12 07:11:16','2017-10-12 04:11:16'),(1782,NULL,'86b374955e6c8e6ac7e69e3d0065886b','b3efda64c23e921249db8d57a2f43db2',16,5,10,10,0,0,1,1,'2017-10-12 07:11:16','2017-10-12 04:11:16'),(1783,NULL,'86b374955e6c8e6ac7e69e3d0065886b','c7b504d4f612323b9338cb20b325a4c3',3,15,2,7,0,0,0,3,'2017-10-12 07:11:16','2017-10-12 04:11:16'),(1784,NULL,'86b374955e6c8e6ac7e69e3d0065886b','c7b504d4f612323b9338cb20b325a4c3',19,22,8,10,0,0,3,0,'2017-10-12 07:11:16','2017-10-12 04:11:16'),(1785,NULL,'86b374955e6c8e6ac7e69e3d0065886b','db61edf4d236769263ba77afc6469271',20,26,5,11,0,0,3,0,'2017-10-12 07:11:16','2017-10-12 04:11:16'),(1786,NULL,'86b374955e6c8e6ac7e69e3d0065886b','db61edf4d236769263ba77afc6469271',21,32,12,1,0,0,3,0,'2017-10-12 07:11:16','2017-10-12 04:11:16'),(1787,NULL,'86b374955e6c8e6ac7e69e3d0065886b','5d23e69afdc164af4e687c98a3f8b3c4',13,31,11,6,0,0,0,3,'2017-10-12 07:11:16','2017-10-12 04:11:16'),(1788,NULL,'86b374955e6c8e6ac7e69e3d0065886b','5d23e69afdc164af4e687c98a3f8b3c4',23,28,7,8,0,0,0,3,'2017-10-12 07:11:16','2017-10-12 04:11:16'),(1789,'f19fc37ab609453d950f3cee0f9a3144','86b374955e6c8e6ac7e69e3d0065886b','bdfce616335398ae06e8663ce775920e',1,24,0,1,0,0,3,4,'2017-10-12 07:11:16','2017-10-12 04:11:16'),(1790,'2f154d7d0a4770c863a560b0539ea8aa','86b374955e6c8e6ac7e69e3d0065886b','bdfce616335398ae06e8663ce775920e',12,6,8,7,0,0,6,3,'2017-10-12 07:11:16','2017-10-12 04:11:16'),(1791,'cec07d2697e94e6857f18c54db0c724e','86b374955e6c8e6ac7e69e3d0065886b','bdfce616335398ae06e8663ce775920e',14,27,2,0,0,0,6,3,'2017-10-12 07:11:16','2017-10-12 04:11:16'),(1792,'062b587509d6b9088a2b33b52a997467','86b374955e6c8e6ac7e69e3d0065886b','bdfce616335398ae06e8663ce775920e',11,10,11,3,0,0,3,6,'2017-10-12 07:11:16','2017-10-12 04:11:16'),(1793,'b3efda64c23e921249db8d57a2f43db2','86b374955e6c8e6ac7e69e3d0065886b','bdfce616335398ae06e8663ce775920e',16,4,0,8,0,0,1,6,'2017-10-12 07:11:16','2017-10-12 04:11:16'),(1794,'c7b504d4f612323b9338cb20b325a4c3','86b374955e6c8e6ac7e69e3d0065886b','bdfce616335398ae06e8663ce775920e',15,19,5,2,0,0,6,3,'2017-10-12 07:11:16','2017-10-12 04:11:16'),(1795,'db61edf4d236769263ba77afc6469271','86b374955e6c8e6ac7e69e3d0065886b','bdfce616335398ae06e8663ce775920e',20,21,4,10,0,0,6,3,'2017-10-12 07:11:16','2017-10-12 04:11:16'),(1796,'5d23e69afdc164af4e687c98a3f8b3c4','86b374955e6c8e6ac7e69e3d0065886b','bdfce616335398ae06e8663ce775920e',31,28,2,3,0,0,3,6,'2017-10-12 07:11:16','2017-10-12 04:11:16'),(1797,NULL,'da4d939c3ac3f045fe39fc11932f706f','f500bd22336af03e741e8dee8dda6109',4,15,0,0,0,0,0,0,NULL,'2017-10-12 10:38:17'),(1798,NULL,'da4d939c3ac3f045fe39fc11932f706f','f500bd22336af03e741e8dee8dda6109',5,1,0,0,0,0,0,0,NULL,'2017-10-12 10:38:17'),(1799,NULL,'da4d939c3ac3f045fe39fc11932f706f','1594e816466fad41ca04e4aa13d3d900',19,25,0,0,0,0,0,0,NULL,'2017-10-12 10:38:17'),(1800,NULL,'da4d939c3ac3f045fe39fc11932f706f','1594e816466fad41ca04e4aa13d3d900',14,24,0,0,0,0,0,0,NULL,'2017-10-12 10:38:18'),(1801,NULL,'da4d939c3ac3f045fe39fc11932f706f','d9ad9822eaabcbd62277b9ccd113e067',23,26,0,0,0,0,0,0,NULL,'2017-10-12 10:38:18'),(1802,NULL,'da4d939c3ac3f045fe39fc11932f706f','d9ad9822eaabcbd62277b9ccd113e067',9,18,0,0,0,0,0,0,NULL,'2017-10-12 10:38:18'),(1803,NULL,'da4d939c3ac3f045fe39fc11932f706f','78baa774e6f7251a1b7ffcadc2a49137',13,2,0,0,0,0,0,0,NULL,'2017-10-12 10:38:18'),(1804,NULL,'da4d939c3ac3f045fe39fc11932f706f','78baa774e6f7251a1b7ffcadc2a49137',12,8,0,0,0,0,0,0,NULL,'2017-10-12 10:38:18'),(1805,NULL,'da4d939c3ac3f045fe39fc11932f706f','8ac41fabf49ceab74a607def099cd616',30,10,0,0,0,0,0,0,NULL,'2017-10-12 10:38:18'),(1806,NULL,'da4d939c3ac3f045fe39fc11932f706f','8ac41fabf49ceab74a607def099cd616',21,27,0,0,0,0,0,0,NULL,'2017-10-12 10:38:18'),(1807,NULL,'da4d939c3ac3f045fe39fc11932f706f','a34df0840eac0a1be1968cfd160c1a0d',20,11,0,0,0,0,0,0,NULL,'2017-10-12 10:38:18'),(1808,NULL,'da4d939c3ac3f045fe39fc11932f706f','a34df0840eac0a1be1968cfd160c1a0d',22,6,0,0,0,0,0,0,NULL,'2017-10-12 10:38:18'),(1809,NULL,'da4d939c3ac3f045fe39fc11932f706f','2561ca954a2c10642ae5f3db4ef26734',7,29,0,0,0,0,0,0,NULL,'2017-10-12 10:38:18'),(1810,NULL,'da4d939c3ac3f045fe39fc11932f706f','2561ca954a2c10642ae5f3db4ef26734',28,32,0,0,0,0,0,0,NULL,'2017-10-12 10:38:19'),(1811,NULL,'da4d939c3ac3f045fe39fc11932f706f','f04d8483644274cd5690eda58eba2a48',17,3,0,0,0,0,0,0,NULL,'2017-10-12 10:38:19'),(1812,NULL,'da4d939c3ac3f045fe39fc11932f706f','f04d8483644274cd5690eda58eba2a48',16,31,0,0,0,0,0,0,NULL,'2017-10-12 10:38:19'),(1813,NULL,'525054d71ebdd9262e3b3c8137a4cc1c','2004ac7fe6b3764938995ad79a84812b',3,23,0,0,0,0,0,0,NULL,'2017-10-13 06:33:06'),(1814,NULL,'525054d71ebdd9262e3b3c8137a4cc1c','2004ac7fe6b3764938995ad79a84812b',22,20,0,0,0,0,0,0,NULL,'2017-10-13 06:33:06'),(1815,NULL,'525054d71ebdd9262e3b3c8137a4cc1c','1bd4e15516c0869be9cca65693a1cad6',30,11,0,0,0,0,0,0,NULL,'2017-10-13 06:33:06'),(1816,NULL,'525054d71ebdd9262e3b3c8137a4cc1c','1bd4e15516c0869be9cca65693a1cad6',28,4,0,0,0,0,0,0,NULL,'2017-10-13 06:33:06'),(1817,NULL,'525054d71ebdd9262e3b3c8137a4cc1c','61e3fe134b9caa7d09ab9f93ea5a07d7',6,7,0,0,0,0,0,0,NULL,'2017-10-13 06:33:06'),(1818,NULL,'525054d71ebdd9262e3b3c8137a4cc1c','61e3fe134b9caa7d09ab9f93ea5a07d7',24,29,0,0,0,0,0,0,NULL,'2017-10-13 06:33:06'),(1819,NULL,'525054d71ebdd9262e3b3c8137a4cc1c','78f62a5a317dd5a01b426123003183bb',21,13,0,0,0,0,0,0,NULL,'2017-10-13 06:33:06'),(1820,NULL,'525054d71ebdd9262e3b3c8137a4cc1c','78f62a5a317dd5a01b426123003183bb',31,5,0,0,0,0,0,0,NULL,'2017-10-13 06:33:06'),(1821,NULL,'525054d71ebdd9262e3b3c8137a4cc1c','6c1d0b1928fc7748721ce047278bc5f1',9,8,0,0,0,0,0,0,NULL,'2017-10-13 06:33:06'),(1822,NULL,'525054d71ebdd9262e3b3c8137a4cc1c','6c1d0b1928fc7748721ce047278bc5f1',17,16,0,0,0,0,0,0,NULL,'2017-10-13 06:33:06'),(1823,NULL,'525054d71ebdd9262e3b3c8137a4cc1c','c710835a72038c941c86091b82f1a884',15,2,0,0,0,0,0,0,NULL,'2017-10-13 06:33:06'),(1824,NULL,'525054d71ebdd9262e3b3c8137a4cc1c','c710835a72038c941c86091b82f1a884',14,18,0,0,0,0,0,0,NULL,'2017-10-13 06:33:06'),(1825,NULL,'525054d71ebdd9262e3b3c8137a4cc1c','e3740a8a090658f38136ee89028df64e',1,19,0,0,0,0,0,0,NULL,'2017-10-13 06:33:06'),(1826,NULL,'525054d71ebdd9262e3b3c8137a4cc1c','e3740a8a090658f38136ee89028df64e',26,25,0,0,0,0,0,0,NULL,'2017-10-13 06:33:06'),(1827,NULL,'525054d71ebdd9262e3b3c8137a4cc1c','b56fc7e212f652447af28a337aaab24a',10,12,0,0,0,0,0,0,NULL,'2017-10-13 06:33:06'),(1828,NULL,'525054d71ebdd9262e3b3c8137a4cc1c','b56fc7e212f652447af28a337aaab24a',32,27,0,0,0,0,0,0,NULL,'2017-10-13 06:33:06'),(1829,NULL,'332ec6c2b13da244510fdaca69aca085','79fc473120363466730031c4a8abea9c',13,21,5,8,0,0,0,3,'2017-10-13 09:46:03','2017-10-13 06:46:03'),(1830,NULL,'332ec6c2b13da244510fdaca69aca085','79fc473120363466730031c4a8abea9c',17,22,2,7,0,0,0,3,'2017-10-13 09:46:03','2017-10-13 06:46:03'),(1831,NULL,'332ec6c2b13da244510fdaca69aca085','885031e9f5df17d932cefbaf5533a3a0',7,8,7,7,0,0,1,1,'2017-10-13 09:46:03','2017-10-13 06:46:03'),(1832,NULL,'332ec6c2b13da244510fdaca69aca085','885031e9f5df17d932cefbaf5533a3a0',16,18,2,6,0,0,0,3,'2017-10-13 09:46:03','2017-10-13 06:46:03'),(1833,NULL,'332ec6c2b13da244510fdaca69aca085','043c2b7b30be0a7c4567b1db1ec5bf32',25,3,5,6,0,0,0,3,'2017-10-13 09:46:03','2017-10-13 06:46:03'),(1834,NULL,'332ec6c2b13da244510fdaca69aca085','043c2b7b30be0a7c4567b1db1ec5bf32',31,20,10,3,0,0,0,3,'2017-10-13 09:46:03','2017-10-13 06:46:03'),(1835,NULL,'332ec6c2b13da244510fdaca69aca085','1c3e9a41c8abc6e260c67fbf14bada04',9,14,5,7,0,0,0,3,'2017-10-13 09:46:04','2017-10-13 06:46:04'),(1836,NULL,'332ec6c2b13da244510fdaca69aca085','1c3e9a41c8abc6e260c67fbf14bada04',6,23,0,6,0,0,0,3,'2017-10-13 09:46:04','2017-10-13 06:46:04'),(1837,NULL,'332ec6c2b13da244510fdaca69aca085','052d7c172a56cd98880e8f10cc63067f',24,27,5,11,0,0,3,0,'2017-10-13 09:46:04','2017-10-13 06:46:04'),(1838,NULL,'332ec6c2b13da244510fdaca69aca085','052d7c172a56cd98880e8f10cc63067f',19,2,9,7,0,0,3,0,'2017-10-13 09:46:04','2017-10-13 06:46:04'),(1839,NULL,'332ec6c2b13da244510fdaca69aca085','98256923303f97f35281a7400ce34d40',1,5,10,10,0,0,1,1,'2017-10-13 09:46:04','2017-10-13 06:46:04'),(1840,NULL,'332ec6c2b13da244510fdaca69aca085','98256923303f97f35281a7400ce34d40',10,28,1,7,0,0,0,3,'2017-10-13 09:46:04','2017-10-13 06:46:04'),(1841,NULL,'332ec6c2b13da244510fdaca69aca085','6da0baa4eecb45982d65ccef98f4eb07',4,15,5,9,0,0,0,3,'2017-10-13 09:46:04','2017-10-13 06:46:04'),(1842,NULL,'332ec6c2b13da244510fdaca69aca085','6da0baa4eecb45982d65ccef98f4eb07',30,32,3,0,0,0,3,0,'2017-10-13 09:46:04','2017-10-13 06:46:04'),(1843,NULL,'332ec6c2b13da244510fdaca69aca085','f8addc3eaf5e8475e11cd8c0d325eab8',26,12,9,1,0,0,3,0,'2017-10-13 09:46:05','2017-10-13 06:46:05'),(1844,NULL,'332ec6c2b13da244510fdaca69aca085','f8addc3eaf5e8475e11cd8c0d325eab8',11,29,10,6,0,0,0,3,'2017-10-13 09:46:04','2017-10-13 06:46:04'),(1845,'79fc473120363466730031c4a8abea9c','332ec6c2b13da244510fdaca69aca085','99a698f818596205c50d40d4778a3eb7',21,22,7,10,0,0,6,3,'2017-10-13 09:46:05','2017-10-13 06:46:05'),(1846,'885031e9f5df17d932cefbaf5533a3a0','332ec6c2b13da244510fdaca69aca085','99a698f818596205c50d40d4778a3eb7',7,18,9,2,0,0,4,3,'2017-10-13 09:46:05','2017-10-13 06:46:05'),(1847,'043c2b7b30be0a7c4567b1db1ec5bf32','332ec6c2b13da244510fdaca69aca085','99a698f818596205c50d40d4778a3eb7',3,20,10,7,0,0,3,6,'2017-10-13 09:46:05','2017-10-13 06:46:05'),(1848,'1c3e9a41c8abc6e260c67fbf14bada04','332ec6c2b13da244510fdaca69aca085','99a698f818596205c50d40d4778a3eb7',23,14,8,5,0,0,6,3,'2017-10-13 09:46:05','2017-10-13 06:46:05'),(1849,'052d7c172a56cd98880e8f10cc63067f','332ec6c2b13da244510fdaca69aca085','99a698f818596205c50d40d4778a3eb7',19,24,2,9,0,0,3,6,'2017-10-13 09:46:05','2017-10-13 06:46:05'),(1850,'98256923303f97f35281a7400ce34d40','332ec6c2b13da244510fdaca69aca085','99a698f818596205c50d40d4778a3eb7',1,28,8,3,0,0,4,3,'2017-10-13 09:46:05','2017-10-13 06:46:05'),(1851,'6da0baa4eecb45982d65ccef98f4eb07','332ec6c2b13da244510fdaca69aca085','99a698f818596205c50d40d4778a3eb7',15,30,9,0,0,0,6,3,'2017-10-13 09:46:05','2017-10-13 06:46:05'),(1852,'f8addc3eaf5e8475e11cd8c0d325eab8','332ec6c2b13da244510fdaca69aca085','99a698f818596205c50d40d4778a3eb7',29,26,7,8,0,0,3,6,'2017-10-13 09:46:05','2017-10-13 06:46:05'),(1853,'99a698f818596205c50d40d4778a3eb7','332ec6c2b13da244510fdaca69aca085','af24828bcfc259f76cf69bffeef9b9fa',23,7,0,0,0,0,7,5,'2017-10-13 09:46:16','2017-10-13 06:46:16'),(1854,'99a698f818596205c50d40d4778a3eb7','332ec6c2b13da244510fdaca69aca085','af24828bcfc259f76cf69bffeef9b9fa',20,21,9,8,0,0,9,6,'2017-10-13 09:46:16','2017-10-13 06:46:16'),(1855,'99a698f818596205c50d40d4778a3eb7','332ec6c2b13da244510fdaca69aca085','af24828bcfc259f76cf69bffeef9b9fa',24,26,2,4,0,0,6,9,'2017-10-13 09:46:16','2017-10-13 06:46:16'),(1856,'99a698f818596205c50d40d4778a3eb7','332ec6c2b13da244510fdaca69aca085','af24828bcfc259f76cf69bffeef9b9fa',1,15,8,11,0,0,7,6,'2017-10-13 09:46:16','2017-10-13 06:46:16'),(1857,'af24828bcfc259f76cf69bffeef9b9fa','332ec6c2b13da244510fdaca69aca085','c1ec9072ae583b9ae6cd3002bb5088e7',1,20,10,3,0,0,7,12,'2017-10-13 09:46:22','2017-10-13 06:46:22'),(1858,'af24828bcfc259f76cf69bffeef9b9fa','332ec6c2b13da244510fdaca69aca085','c1ec9072ae583b9ae6cd3002bb5088e7',23,26,6,3,0,0,10,9,'2017-10-13 09:46:23','2017-10-13 06:46:23');
/*!40000 ALTER TABLE `competition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team`
--

DROP TABLE IF EXISTS `team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team` (
  `id` tinyint(3) unsigned NOT NULL COMMENT 'идентификатор',
  `country` varchar(20) NOT NULL COMMENT 'страна',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='команда';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team`
--

LOCK TABLES `team` WRITE;
/*!40000 ALTER TABLE `team` DISABLE KEYS */;
INSERT INTO `team` VALUES (1,'Бразилия'),(2,'Германия / ФРГ'),(3,'Италия'),(4,'Аргентина'),(5,'Англия'),(6,'Испания'),(7,'Франция'),(8,'Голландия'),(9,'Уругвай'),(10,'Швеция'),(11,'Россия / СССР'),(12,'Сербия / Югославия'),(13,'Мексика'),(14,'Бельгия'),(15,'Польша'),(16,'Венгрия'),(17,'Португалия'),(18,'Чехия / Чехословакия'),(19,'Чили'),(20,'Австрия'),(21,'Швейцария'),(22,'Парагвай'),(23,'США'),(24,'Румыния'),(25,'Южная Корея'),(26,'Дания'),(27,'Хорватия'),(28,'Колумбия'),(29,'Шотландия'),(30,'Камерун'),(31,'Коста-Рика'),(32,'Болгария'),(33,'Нигерия'),(34,'Ирландия'),(35,'Япония'),(36,'Турция'),(37,'Гана'),(38,'Сев. Ирландия'),(39,'Перу'),(40,'Эквадор'),(41,'Алжир'),(42,'ЮАР'),(43,'Марокко'),(44,'Кот-д\'Ивуар'),(45,'Норвегия'),(46,'Австралия'),(47,'Сенегал'),(48,'ГДР'),(49,'Тунис'),(50,'Греция'),(51,'Саудовская Аравия'),(52,'Уэльс'),(53,'Украина'),(54,'Иран'),(55,'Словакия'),(56,'Словения'),(57,'Куба'),(58,'Новая Зеландия'),(59,'Гондурас'),(60,'КНДР'),(61,'Босния и Герцеговина'),(62,'Ангола'),(63,'Израиль'),(64,'Египет'),(65,'Ямайка'),(66,'Кувейт'),(67,'Тринидад и Тобаго'),(68,'Боливия'),(69,'Ирак'),(70,'Того'),(71,'Канада'),(72,'Гол. Вест-Индия'),(73,'ОАЭ'),(74,'Китай'),(75,'Гаити'),(76,'Заир'),(77,'Сальвадор');
/*!40000 ALTER TABLE `team` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team_history`
--

DROP TABLE IF EXISTS `team_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team_history` (
  `team_id` tinyint(3) unsigned NOT NULL COMMENT 'идентификатор',
  `parts` tinyint(3) unsigned NOT NULL COMMENT 'кол-во участий в турнире',
  `games` tinyint(3) unsigned NOT NULL COMMENT 'кол-во сыгранных игр',
  `wins` tinyint(3) unsigned NOT NULL COMMENT 'побед',
  `draws` tinyint(3) unsigned NOT NULL COMMENT 'ничей',
  `defeats` tinyint(3) unsigned NOT NULL COMMENT 'поражений',
  `scored` tinyint(3) unsigned NOT NULL COMMENT 'забито',
  `missed` tinyint(3) unsigned NOT NULL COMMENT 'пропущено',
  `scores` tinyint(3) unsigned NOT NULL COMMENT 'кол-во очков',
  `percent` tinyint(2) unsigned NOT NULL,
  `firsts` tinyint(3) unsigned NOT NULL COMMENT 'сколько раз были первыми',
  `seconds` tinyint(3) unsigned NOT NULL COMMENT 'сколько раз были вторыми',
  `thirds` tinyint(3) unsigned NOT NULL COMMENT 'сколько раз были третьими',
  PRIMARY KEY (`team_id`),
  CONSTRAINT `fk_team_history_team_id_team` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='исторические данные команды';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team_history`
--

LOCK TABLES `team_history` WRITE;
/*!40000 ALTER TABLE `team_history` DISABLE KEYS */;
INSERT INTO `team_history` VALUES (1,20,104,70,17,17,127,102,157,76,5,2,2),(2,18,106,66,20,20,127,121,152,72,4,4,4),(3,18,83,45,21,17,127,77,111,67,4,2,1),(4,16,77,42,14,21,127,84,98,64,2,3,0),(5,14,62,26,20,16,79,56,72,58,1,0,0),(6,14,59,29,12,18,92,66,70,59,1,0,0),(7,14,59,28,12,19,106,71,68,58,1,1,2),(8,10,50,27,12,11,86,48,66,66,0,3,1),(9,12,51,20,12,19,80,71,52,51,2,0,0),(10,11,46,16,13,17,74,69,45,49,0,1,2),(11,10,40,17,8,15,66,47,42,53,0,0,0),(12,11,43,17,8,18,64,59,42,49,0,0,0),(13,15,53,14,14,25,57,92,42,40,0,0,0),(14,12,41,14,9,18,52,66,37,45,0,0,0),(15,7,31,15,5,11,44,40,35,57,0,0,2),(16,9,32,15,3,14,87,57,33,52,0,2,0),(17,6,26,13,4,9,43,29,30,58,0,0,1),(18,9,33,12,5,16,47,49,29,44,0,2,0),(19,9,33,11,7,15,40,49,29,44,0,0,1),(20,7,29,12,4,13,43,47,28,48,0,0,1),(21,10,33,11,6,16,45,59,28,42,0,0,0),(22,8,27,7,10,10,30,38,24,44,0,0,0),(23,10,33,8,6,19,37,62,22,33,0,0,1),(24,7,21,8,5,8,30,32,21,50,0,0,0),(25,9,31,5,9,17,31,67,19,31,0,0,0),(26,4,16,8,2,6,27,24,18,56,0,0,0),(27,4,16,7,2,7,21,17,16,50,0,0,1),(28,5,18,7,2,9,26,27,16,44,0,0,0),(29,8,23,4,7,12,25,41,15,33,0,0,0),(30,7,23,4,7,12,18,43,15,33,0,0,0),(31,4,15,5,4,6,17,23,14,47,0,0,0),(32,7,26,3,8,15,22,53,14,27,0,0,0),(33,5,18,5,3,10,20,26,13,36,0,0,0),(34,3,13,2,8,3,10,10,12,46,0,0,0),(35,5,17,4,4,9,14,22,12,35,0,0,0),(36,2,10,5,1,4,20,17,11,55,0,0,1),(37,3,12,4,3,5,13,16,11,46,0,0,0),(38,3,13,3,5,5,13,23,11,42,0,0,0),(39,4,15,4,3,8,19,31,11,37,0,0,0),(40,3,10,4,1,5,10,11,9,45,0,0,0),(41,4,13,3,3,7,13,19,9,35,0,0,0),(42,3,9,2,4,3,11,16,8,44,0,0,0),(43,4,13,2,4,7,12,18,8,31,0,0,0),(44,3,9,3,1,5,13,14,7,39,0,0,0),(45,3,8,2,3,3,7,8,7,44,0,0,0),(46,4,13,2,3,8,11,26,7,27,0,0,0),(47,1,5,2,2,1,7,6,6,60,0,0,0),(48,1,6,2,2,2,5,5,6,50,0,0,0),(49,4,12,1,4,7,8,17,6,25,0,0,0),(50,3,10,2,2,6,5,20,6,30,0,0,0),(51,4,13,2,2,9,9,32,6,23,0,0,0),(52,1,5,1,3,1,4,4,5,50,0,0,0),(53,1,5,2,1,2,5,7,5,50,0,0,0),(54,4,12,1,3,8,7,22,5,21,0,0,0),(55,1,4,1,1,2,5,7,3,38,0,0,0),(56,2,6,1,1,4,5,10,3,25,0,0,0),(57,1,3,1,1,1,5,12,3,50,0,0,0),(58,2,6,0,3,3,4,14,3,25,0,0,0),(59,3,9,0,3,6,3,14,3,17,0,0,0),(60,2,7,1,1,5,6,21,3,21,0,0,0),(61,1,3,1,0,2,4,4,2,33,0,0,0),(62,1,3,0,2,1,1,2,2,33,0,0,0),(63,1,3,0,2,1,1,3,2,33,0,0,0),(64,2,4,0,2,2,3,6,2,25,0,0,0),(65,1,3,1,0,2,3,9,2,33,0,0,0),(66,1,3,0,1,2,2,6,1,17,0,0,0),(67,1,3,0,1,2,0,4,1,17,0,0,0),(68,3,6,0,1,5,1,20,1,8,0,0,0),(69,1,3,0,0,3,1,4,0,0,0,0,0),(70,1,3,0,0,3,1,6,0,0,0,0,0),(71,1,3,0,0,3,0,5,0,0,0,0,0),(72,1,1,0,0,1,0,6,0,0,0,0,0),(73,1,3,0,0,3,2,11,0,0,0,0,0),(74,1,3,0,0,3,0,9,0,0,0,0,0),(75,1,3,0,0,3,2,14,0,0,0,0,0),(76,1,3,0,0,3,0,14,0,0,0,0,0),(77,2,6,0,0,6,1,22,0,0,0,0,0);
/*!40000 ALTER TABLE `team_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `v_championship`
--

DROP TABLE IF EXISTS `v_championship`;
/*!50001 DROP VIEW IF EXISTS `v_championship`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_championship` AS SELECT 
 1 AS `championship_id`,
 1 AS `group_id`,
 1 AS `parent_id`,
 1 AS `played`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_competition`
--

DROP TABLE IF EXISTS `v_competition`;
/*!50001 DROP VIEW IF EXISTS `v_competition`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_competition` AS SELECT 
 1 AS `team_id`,
 1 AS `games`,
 1 AS `scored`,
 1 AS `missed`,
 1 AS `scores`,
 1 AS `wins`,
 1 AS `draws`,
 1 AS `defeats`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_group`
--

DROP TABLE IF EXISTS `v_group`;
/*!50001 DROP VIEW IF EXISTS `v_group`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_group` AS SELECT 
 1 AS `group_id`,
 1 AS `played`,
 1 AS `teamA_id`,
 1 AS `teamA_country`,
 1 AS `teamA_goals`,
 1 AS `teamA_scored_missed`,
 1 AS `teamB_id`,
 1 AS `teamB_country`,
 1 AS `teamB_goals`,
 1 AS `teamB_scored_missed`,
 1 AS `teamA_scores`,
 1 AS `teamB_scores`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_group_winner`
--

DROP TABLE IF EXISTS `v_group_winner`;
/*!50001 DROP VIEW IF EXISTS `v_group_winner`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_group_winner` AS SELECT 
 1 AS `group_id`,
 1 AS `teamA_country`,
 1 AS `teamA_goals`,
 1 AS `teamA_id`,
 1 AS `teamB_id`,
 1 AS `teamB_country`,
 1 AS `teamB_goals`,
 1 AS `teamA_scores`,
 1 AS `teamB_scores`,
 1 AS `teamA_scored_missed`,
 1 AS `teamB_scored_missed`,
 1 AS `teamA_scores_total`,
 1 AS `teamB_scores_total`,
 1 AS `winner_letter`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_team`
--

DROP TABLE IF EXISTS `v_team`;
/*!50001 DROP VIEW IF EXISTS `v_team`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_team` AS SELECT 
 1 AS `id`,
 1 AS `country`,
 1 AS `team_id`,
 1 AS `games`,
 1 AS `scored`,
 1 AS `missed`,
 1 AS `scored_missed`,
 1 AS `scores`,
 1 AS `wins`,
 1 AS `draws`,
 1 AS `defeats`,
 1 AS `attack_power`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_team_history`
--

DROP TABLE IF EXISTS `v_team_history`;
/*!50001 DROP VIEW IF EXISTS `v_team_history`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_team_history` AS SELECT 
 1 AS `team_id`,
 1 AS `games`,
 1 AS `scored`,
 1 AS `missed`,
 1 AS `scores`,
 1 AS `wins`,
 1 AS `draws`,
 1 AS `defeats`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `v_championship`
--

/*!50001 DROP VIEW IF EXISTS `v_championship`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_championship` AS select sql_small_result `c1`.`championship_id` AS `championship_id`,`c1`.`group_id` AS `group_id`,`c1`.`parent_id` AS `parent_id`,`c1`.`played` AS `played` from `competition` `c1` where (`c1`.`played` is not null) group by `c1`.`championship_id`,`c1`.`group_id`,`c1`.`parent_id`,`c1`.`played` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_competition`
--

/*!50001 DROP VIEW IF EXISTS `v_competition`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_competition` AS select sql_small_result `vc1`.`teamA_id` AS `team_id`,count(`vc1`.`played`) AS `games`,sum(`vc1`.`teamA_goals`) AS `scored`,sum(`vc1`.`teamB_goals`) AS `missed`,sum((case when (`vc1`.`teamA_goals` = `vc1`.`teamB_goals`) then 1 when (`vc1`.`teamA_goals` > `vc1`.`teamB_goals`) then 3 else 0 end)) AS `scores`,sum((`vc1`.`teamA_goals` > `vc1`.`teamB_goals`)) AS `wins`,sum((`vc1`.`teamA_goals` = `vc1`.`teamB_goals`)) AS `draws`,sum((`vc1`.`teamA_goals` < `vc1`.`teamB_goals`)) AS `defeats` from (select `c1`.`teamA_id` AS `teamA_id`,`c1`.`teamA_goals` AS `teamA_goals`,`c1`.`teamB_goals` AS `teamB_goals`,`c1`.`played` AS `played` from `test5`.`competition` `c1` union all select `c1`.`teamB_id` AS `teamB_id`,`c1`.`teamB_goals` AS `teamB_goals`,`c1`.`teamA_goals` AS `teamA_goals`,`c1`.`played` AS `played` from `test5`.`competition` `c1`) `vc1` group by `vc1`.`teamA_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_group`
--

/*!50001 DROP VIEW IF EXISTS `v_group`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_group` AS select sql_small_result sql_cache `c1`.`group_id` AS `group_id`,`c1`.`played` AS `played`,`t1`.`id` AS `teamA_id`,`t1`.`country` AS `teamA_country`,(case when isnull(`c1`.`played`) then NULL else `c1`.`teamA_goals` end) AS `teamA_goals`,(case when isnull(`c1`.`played`) then NULL else (`c1`.`teamA_goals` - `c1`.`teamB_goals`) end) AS `teamA_scored_missed`,`t2`.`id` AS `teamB_id`,`t2`.`country` AS `teamB_country`,(case when isnull(`c1`.`played`) then NULL else `c1`.`teamB_goals` end) AS `teamB_goals`,(case when isnull(`c1`.`played`) then NULL else (`c1`.`teamB_goals` - `c1`.`teamA_goals`) end) AS `teamB_scored_missed`,(case when isnull(`c1`.`played`) then NULL else `c1`.`teamA_scores` end) AS `teamA_scores`,(case when isnull(`c1`.`played`) then NULL else `c1`.`teamB_scores` end) AS `teamB_scores` from ((`competition` `c1` join `team` `t1` on((`c1`.`teamA_id` = `t1`.`id`))) join `team` `t2` on((`c1`.`teamB_id` = `t2`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_group_winner`
--

/*!50001 DROP VIEW IF EXISTS `v_group_winner`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_group_winner` AS select sql_small_result sql_cache `vg1`.`group_id` AS `group_id`,`vg1`.`teamA_country` AS `teamA_country`,`vg1`.`teamA_goals` AS `teamA_goals`,`vg1`.`teamA_id` AS `teamA_id`,`vg1`.`teamB_id` AS `teamB_id`,`vg1`.`teamB_country` AS `teamB_country`,`vg1`.`teamB_goals` AS `teamB_goals`,`vg1`.`teamA_scores` AS `teamA_scores`,`vg1`.`teamB_scores` AS `teamB_scores`,`vg1`.`teamA_scored_missed` AS `teamA_scored_missed`,`vg1`.`teamB_scored_missed` AS `teamB_scored_missed`,`vt1`.`scores` AS `teamA_scores_total`,`vt2`.`scores` AS `teamB_scores_total`,(case when isnull(`vg1`.`played`) then NULL when (`vg1`.`teamA_scores` > `vg1`.`teamB_scores`) then 'A' when (`vg1`.`teamA_scores` < `vg1`.`teamB_scores`) then 'B' when (`vg1`.`teamA_scored_missed` > `vg1`.`teamB_scored_missed`) then 'A' when (`vg1`.`teamA_scored_missed` < `vg1`.`teamB_scored_missed`) then 'B' when (`vt1`.`scores` > `vt2`.`scores`) then 'A' when (`vt1`.`scores` > `vt2`.`scores`) then 'B' when (`vt1`.`id` > `vt2`.`id`) then 'A' else 'B' end) AS `winner_letter` from ((`test5`.`v_group` `vg1` join `test5`.`v_team` `vt1` on((`vg1`.`teamA_id` = `vt1`.`team_id`))) join `test5`.`v_team` `vt2` on((`vg1`.`teamB_id` = `vt2`.`team_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_team`
--

/*!50001 DROP VIEW IF EXISTS `v_team`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_team` AS select sql_small_result `t1`.`id` AS `id`,`t1`.`country` AS `country`,`vth1`.`team_id` AS `team_id`,`vth1`.`games` AS `games`,`vth1`.`scored` AS `scored`,`vth1`.`missed` AS `missed`,(`vth1`.`scored` - `vth1`.`missed`) AS `scored_missed`,`vth1`.`scores` AS `scores`,`vth1`.`wins` AS `wins`,`vth1`.`draws` AS `draws`,`vth1`.`defeats` AS `defeats`,((1.0 + `vth1`.`scored`) / (1.0 + `vth1`.`missed`)) AS `attack_power` from (`test5`.`v_team_history` `vth1` join `test5`.`team` `t1` on((`vth1`.`team_id` = `t1`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_team_history`
--

/*!50001 DROP VIEW IF EXISTS `v_team_history`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_team_history` AS select sql_small_result `th1`.`team_id` AS `team_id`,(`th1`.`games` + coalesce(`vc1`.`games`,0)) AS `games`,(`th1`.`scored` + coalesce(`vc1`.`scored`,0)) AS `scored`,(`th1`.`missed` + coalesce(`vc1`.`missed`,0)) AS `missed`,(`th1`.`scores` + coalesce(`vc1`.`scores`,0)) AS `scores`,(`th1`.`wins` + coalesce(`vc1`.`wins`,0)) AS `wins`,(`th1`.`draws` + coalesce(`vc1`.`draws`,0)) AS `draws`,(`th1`.`defeats` + coalesce(`vc1`.`defeats`,0)) AS `defeats` from (`test5`.`team_history` `th1` left join `test5`.`v_competition` `vc1` on((`th1`.`team_id` = `vc1`.`team_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-24 11:42:58
