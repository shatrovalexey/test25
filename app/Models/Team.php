<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 24 Oct 2017 07:24:01 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Team
 * 
 * @property int $id
 * @property string $country
 * 
 * @property \Illuminate\Database\Eloquent\Collection $competitions
 * @property \App\Models\TeamHistory $team_history
 *
 * @package App\Models
 */
class Team extends Eloquent
{
	protected $table = 'team';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int'
	];

	protected $fillable = [
		'country'
	];

	public function competitions()
	{
		return $this->hasMany(\App\Models\Competition::class, 'teamB_id');
	}

	public function team_history()
	{
		return $this->hasOne(\App\Models\TeamHistory::class);
	}
}
