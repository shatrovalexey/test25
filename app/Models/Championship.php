<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 24 Oct 2017 07:24:01 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Championship
 * 
 * @property string $id
 * @property \Carbon\Carbon $created
 * 
 * @property \Illuminate\Database\Eloquent\Collection $competitions
 *
 * @package App\Models
 */
class Championship extends Eloquent
{
	protected $table = 'championship';
	public $incrementing = false;
	public $timestamps = false;

	protected $dates = [
		'created'
	];

	protected $fillable = [
		'created'
	];

	public function competitions()
	{
		return $this->hasMany(\App\Models\Competition::class);
	}

	/**
     	* Новая запись
     	*
     	* @return string(32) - идентификатор
     	*/
	public static function store( ) {
 	        $self = new self( ) ;
	        $self->id = $self->__id( ) ;
	        $self->save( ) ;

		return $self->id ;
	}

	/**
	* Новый идентификатор
	* @return string(32) - идентификатор
	*/
	private static function __id( ) {
		return md5( uniqid( ) ) ;
	}
}
