<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 24 Oct 2017 07:24:01 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ViewTeam
 * 
 * @property int $id
 * @property string $country
 * @property int $team_id
 * @property int games
 * @property int scored
 * @property int missed
 * @property int scored_missed
 * @property int scores
 * @property int wins
 * @property int draws
 * @property int defeats
 * @property int attack_power
 * 
 * @property \Illuminate\Database\Eloquent\Collection $competitions
 * @property \App\Models\TeamHistory $team_history
 *
 * @package App\Models
 */
class ViewTeam extends Eloquent
{
	protected $table = 'v_team';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int'
	] ;
}
