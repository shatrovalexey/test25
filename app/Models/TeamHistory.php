<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 24 Oct 2017 07:24:01 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TeamHistory
 * 
 * @property int $team_id
 * @property int $parts
 * @property int $games
 * @property int $wins
 * @property int $draws
 * @property int $defeats
 * @property int $scored
 * @property int $missed
 * @property int $scores
 * @property int $percent
 * @property int $firsts
 * @property int $seconds
 * @property int $thirds
 * 
 * @property \App\Models\Team $team
 *
 * @package App\Models
 */
class TeamHistory extends Eloquent
{
	protected $table = 'team_history';
	protected $primaryKey = 'team_id';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'team_id' => 'int',
		'parts' => 'int',
		'games' => 'int',
		'wins' => 'int',
		'draws' => 'int',
		'defeats' => 'int',
		'scored' => 'int',
		'missed' => 'int',
		'scores' => 'int',
		'percent' => 'int',
		'firsts' => 'int',
		'seconds' => 'int',
		'thirds' => 'int'
	];

	protected $fillable = [
		'parts',
		'games',
		'wins',
		'draws',
		'defeats',
		'scored',
		'missed',
		'scores',
		'percent',
		'firsts',
		'seconds',
		'thirds'
	];

	public function team()
	{
		return $this->belongsTo(\App\Models\Team::class);
	}
}
