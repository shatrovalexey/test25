<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 24 Oct 2017 07:24:01 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Competition
 * 
 * @property int $id
 * @property string $parent_id
 * @property string $championship_id
 * @property string $group_id
 * @property int $teamA_id
 * @property int $teamB_id
 * @property int $teamA_goals
 * @property int $teamB_goals
 * @property int $teamA_goals_total
 * @property int $teamB_goals_total
 * @property int $teamA_scores
 * @property int $teamB_scores
 * @property \Carbon\Carbon $played
 * @property \Carbon\Carbon $created
 * 
 * @property \App\Models\Championship $championship
 * @property \App\Models\Team $team
 *
 * @package App\Models
 */
class Competition extends Eloquent
{
	protected $table = 'competition';
	public $timestamps = false;

	protected $casts = [
		'teamA_id' => 'int',
		'teamB_id' => 'int',
		'teamA_goals' => 'int',
		'teamB_goals' => 'int',
		'teamA_goals_total' => 'int',
		'teamB_goals_total' => 'int',
		'teamA_scores' => 'int',
		'teamB_scores' => 'int'
	];

	protected $dates = [
		'played',
		'created'
	];

	protected $fillable = [
		'parent_id',
		'championship_id',
		'group_id',
		'teamA_id',
		'teamB_id',
		'teamA_goals',
		'teamB_goals',
		'teamA_goals_total',
		'teamB_goals_total',
		'teamA_scores',
		'teamB_scores',
		'played',
		'created'
	];

	public function championship()
	{
		return $this->belongsTo(\App\Models\Championship::class);
	}

	public function team()
	{
		return $this->belongsTo(\App\Models\Team::class, 'teamB_id');
	}

	/**
	* Жеребьёвка (заглушка)
	* @param string(32) $championship_id - идентификатор чемпионата
	* @returns Illuminate\Database\Eloquent\Collection - список идентификаторов групп
	*/
	public static function suggest( $championship_id ) {
		return array( ) ;
	}

	/**
	* Список команд группы
	* @param string(32) $group_id - идентификатор группы
	* @returns Illuminate\Database\Eloquent\Collection - список групп
	*/
	public static function groups( $group_id ) {
		return array( ) ;
	}
}
