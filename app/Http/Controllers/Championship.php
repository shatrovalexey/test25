<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ViewTeam as ViewTeam ;
use App\Models\Championship as Championship ;
use App\Models\Competition as Competition ;

class Championship extends Controller
{

	/**
	* Список команд с рейтингом
	* @return Response
	*/
	public function teamlist( ) {
		return ViewTeam::all( )->toJson( ) ;
	}

	/**
	* Жеребьёвка
	* @return array of array - таблица жеребьёвки
	*/
	public function suggest( ) {
		$championship_id = Championship::store( ) ;

		$result = array(
			'championship_id' => $championship_id ,
			'groups' => array( )
		) ;
		foreach ( Competition::suggest( $championship_id ) as $group_id ) {
			$result[ 'groups' ][ $group_id ] = Competition::groups( $group_id ) ;
		}

		return response( )->json( $result ) ;
	}
}
